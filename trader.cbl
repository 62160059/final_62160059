       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER.
       AUTHOR. NATCHAMON.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader9.dat"
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER-FILE.
       01  TRADER-DETAIL.
           88 END-OF-TRADER-FILE VALUE HIGH-VALUE .
           05 PROVINCE          PIC X(2).
           05 MEMBER            PIC X(4).
           05 INCOME            PIC 9(9).
       WORKING-STORAGE SECTION. 
       01  INCOME-TABLE.
           05 INCOME-TOTAL      PIC 9(9).
       01  MEMBER-INCOME        PIC X(4).
       01  PRN-PROVINCE         PIC X(2).  
       01  PRN-MEMBER           PIC X(4).
       01  PRN-INCOME           PIC 9(9).
       
       PROCEDURE DIVISION.
       BEGIN.
           MOVE ZEROS TO INCOME-TABLE
           OPEN INPUT TRADER-FILE 
           PERFORM 001-READ-FILE
           PERFORM 001-PROCESS-HEADER UNTIL END-OF-TRADER-FILE
                    
                


           CLOSE TRADER-FILE
           GOBACK
           .
              
       001-READ-FILE.
           READ TRADER-FILE
               AT END SET END-OF-TRADER-FILE TO TRUE
           END-READ

           .
              
       001-PROCESS-HEADER.
           MOVE PROVINCE TO PRN-PROVINCE 
           MOVE INCOME TO PRN-INCOME
           
              
           .